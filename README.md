# Web Expeditious Retrieval Challenge

The goal of this challenge is to implement a Chrome extension that allows users to search through a shared navigation history.
A minimal skeleton project is provided with a functional Chrome extension and a Python HTTP service (check that you have an updated version of Chrome and Python 3).
All files in this skeleton (including this README) can be changed to implement and document the required features, additional files can also be added as necessary.

The backend service listens for connections at `http://localhost:8888`, responding with a static HTML.
Upon visiting that URL on a Chrome browser with the extension installed an injected alert message should appear.

#### To execute the backend service run:
```
python ./src/server/wer.py
```

#### To install the extension follow these steps:
1. Go to `chrome://extensions` in your Chrome browser.
2. Enable "Developer mode" if not already enabled.
3. Click "Load unpacked" and select the `src/extension` folder from this project.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


### Who do I talk to? ###

* Repo owner or admin: Leandro Nahabedian leanahabedian@gmail.com
